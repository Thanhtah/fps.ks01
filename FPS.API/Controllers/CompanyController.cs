﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPost]
        [Route("AddCompany")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }

        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<CompanyModel> GetAll()
        {
            return _companyRepository.GetAll();
        }

        [HttpGet]
        [Route("GetCompanyById/{id}")]
        public IActionResult GetCompanyById(Guid id)
        {
            try
            {
                var company = _companyRepository.GetCompanyById(id);
                if (company == null)
                {
                    return NotFound($"Company with id {id} not found.");
                }
                return Ok(company);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error retrieving company: {ex.Message}");
            }
        }

        [HttpPost]
        [Route("UpdateCompany/{id}")]
        public IActionResult UpdateCompany(Guid id, CompanyModel model)
        {
            try
            {
                _companyRepository.UpdateCompany(id, model);
                return Ok("Company updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error updating company: {ex.Message}");
            }
        }

        [HttpDelete]
        [Route("DeleteCompany/{id}")]
        public IActionResult DeleteCompany(Guid id)
        {
            try
            {
                _companyRepository.DeleteCompany(id);
                return Ok("Company deleted successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error deleting company: {ex.Message}");
            }
        }
    }
    
   
}


