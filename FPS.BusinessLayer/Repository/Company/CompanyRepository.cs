﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository : BaseRepository, ICompanyRepository 
    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }

        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreatedDate = DateTime.Now;
            company.UpdatedDate = DateTime.Now;
            company.CreatedBy = model.CreatedBy;
            company.UpdatedBy = model.UpdatedBy; 
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }
        public void UpdateCompany(CompanyModel model)
        {
            var companyToUpdate = _context.Companies.FirstOrDefault(c => c.Id == model.Id);

            if (companyToUpdate != null)
            {
                companyToUpdate.CompanyName = model.CompanyName;
                companyToUpdate.UpdatedDate = DateTime.Now;
                companyToUpdate.UpdatedBy = model.UpdatedBy;

                _context.SaveChanges();
            }
        }

        public IEnumerable<CompanyModel> GetAll()
        {
            var companies = _context.Companies.Select(company => new CompanyModel
            {
                Id = company.Id,
                CompanyName = company.CompanyName,
                CreatedDate = company.CreatedDate,
                UpdatedDate = company.UpdatedDate,
                CreatedBy = company.CreatedBy,
                UpdatedBy = company.UpdatedBy,
                IsDeleted = company.IsDeleted
            }).ToList();

            return companies;
        }

        public CompanyModel GetCompanyById(Guid id)
        {
            var company = _context.Companies.FirstOrDefault(c => c.Id == id);
            if (company == null)
                return null;

            return new CompanyModel
            {
                Id = company.Id,
                CompanyName = company.CompanyName,
                CreatedDate = company.CreatedDate,
                UpdatedDate = company.UpdatedDate,
                CreatedBy = company.CreatedBy,
                UpdatedBy = company.UpdatedBy,
                IsDeleted = company.IsDeleted
            };
        }

        public void UpdateCompany(Guid id, CompanyModel model)
        {
            var companyToUpdate = _context.Companies.FirstOrDefault(c => c.Id == id);

            if (companyToUpdate != null)
            {
                companyToUpdate.CompanyName = model.CompanyName;
                companyToUpdate.UpdatedDate = DateTime.Now;
                companyToUpdate.UpdatedBy = model.UpdatedBy;

                _context.SaveChanges();
            }
        }
        public void DeleteCompany(Guid id)
        {
            var companyToDelete = _context.Companies.FirstOrDefault(c => c.Id == id);

            if (companyToDelete != null)
            {
                _context.Companies.Remove(companyToDelete);
                _context.SaveChanges();
            }
        }
    }
}
